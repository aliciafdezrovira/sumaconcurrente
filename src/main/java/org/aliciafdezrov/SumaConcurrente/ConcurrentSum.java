package org.aliciafdezrov.SumaConcurrente;

import org.aliciafdezrov.SumaConcurrente.SumarSeq;

public class ConcurrentSum extends Thread{

	int[] vector;
	 int inicio,fin;
	 double suma;
	 
	 public ConcurrentSum(int[] vector,int i,int j){
	 this.vector = vector;
	 inicio = i;
	 fin = j;
	 }
	 public void run() {
		 
    SumarSeq sum = new SumarSeq();
	 suma = sum.suma(vector, inicio, fin);
	 }
	 
	 public double getSuma(){
		 return suma;
	 }
}
