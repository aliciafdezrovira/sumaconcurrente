package org.aliciafdezrov.SumaConcurrente;

import java.util.Random;

public class Secuencial {

	  public static void main(String[] args) {
	    int[] vector = new int[10000000];
	    Random r = new Random(5);
	    for (int i = 0; i < vector.length; i++)
	      vector[i] = r.nextInt(50000);

	    double sum = 0 ;
	    long initTime = System.currentTimeMillis();
	    for (double value : vector ) {
	      sum += value ;

	      double delay = 0.0 ;
	      for (long i = 0 ; i < 5; i++) {
	        delay = Math.sin(i) ;
	      }
	    }
	    long computingTime = System.currentTimeMillis() - initTime ;

	    System.out.println("Tiempo de suma: " + computingTime);
	    System.out.println("Suma: " + sum);
	  }
	}


