package org.aliciafdezrov.SumaConcurrente;

public class SumarSeq {

	public static double suma(int[] vector, int inicio,int fin){
		 double suma = 0;
		 for (int i = inicio; i<fin; i++){
			 suma += vector[i];
		 }
	return suma;
}
}
